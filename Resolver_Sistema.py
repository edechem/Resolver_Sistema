#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 18 11:06:27 2018

@author: ederson
"""

import math
from scipy.optimize import fsolve

def sistema(p):
    x, y = p
    f1 = x+y**2-4
    f2 = math.exp(x)+x*y-3
    return f1, f2

x0 = 1
y0 = 2

p0 = [x0, y0]

p = fsolve(sistema, p0)

print(p)
