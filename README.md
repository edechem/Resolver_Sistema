O código é apenas um exemplo simples, em Python, de como resolver 
um sistema de equações não lineares.

O código foi testado usando Python 3.6 + Spyder.

O problema foi obtido de: 
https://stackoverflow.com/questions/8739227/how-to-solve-a-pair-of-nonlinear-equations-using-python

Éderson, 18/01/2018
